#!/usr/bin/env bash
#SBATCH --job-name=ppo50gat
#SBATCH --output=ppo50gat%j.log
#SBATCH --error=ppo50gat%j.err
#SBATCH --mail-user=alhermi@uni-hildesheim.de
#SBATCH --partition=STUD
#SBATCH --gres=gpu:1

#!/usr/bin/env bash


DEVICES="0"

wandb login 9815f3b0e8ce08b8878d8ca9a84bca3dd70f3978
wandb offline

CUDA_VISIBLE_DEVICES="$DEVICES" python ppo50_gat.py
